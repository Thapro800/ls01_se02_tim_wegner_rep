
public class LadungTest {

	public static void main(String[] args) {
		
		//LADUNG KLINGONEN
		
		Ladung l1 = new Ladung();
		l1.setBezeichnung("Ferengi Schneckensaft");
		l1.setMenge(200);
		
		Ladung l2 = new Ladung();
		l2.setBezeichnung("Bat'leth Klingonen Schwert");
		l2.setMenge(200);
		
		
		//LADUNG ROMULANER
		
		Ladung l3 = new Ladung();
		l3.setBezeichnung("Borg-Schrott");
		l3.setMenge(5);
		
		Ladung l4 = new Ladung();
		l4.setBezeichnung("Rote Materie");
		l4.setMenge(2);

		Ladung l5 = new Ladung();
		l5.setBezeichnung("Plasma-Waffe");
		l5.setMenge(50);
		
		
		//LADUNG VULKANIER
		
		Ladung l6 = new Ladung();
		l6.setBezeichnung("Forschungssonden");
		l6.setMenge(35);
		
		Ladung l7 = new Ladung();
		l7.setBezeichnung("Photonentorpedo");
		l7.setMenge(3);

	}

}
