
public class RaumschiffTest {

	public static void main(String[] args) {
		
		//KLINGONEN RAUMSCHIFF
		
		Raumschiff r1 = new Raumschiff();

		r1.setPhotonentorpedoAnzahl(1);
		r1.setEnergieversorgungInProzent(100);
		r1.setSchildeInProzent(100);
		r1.setHuelleInProzent(100);
		r1.setLebenserhaltungssystemeInProzent(100);
		r1.setAndroidenAnzahl(2);
		r1.setSchiffname("ISK Hegh'ta");

		System.out.println("Photonentorpedos: " + r1.getPhotonentorpedoAnzahl());
		System.out.println("Energieversorgung in Prozent: " + r1.getEnergieversorgungInProzent());
		System.out.println("Schilde in Prozent: " + r1.getSchildeInProzent());
		System.out.println("Huelle in Prozent: " + r1.getHuelleInProzent());
		System.out.println("Lebenserhaltungssyteme in Prozent: " + r1.getLebenserhaltungssystemeInProzent());
		System.out.println("Androiden Anzahl: " + r1.getAndroidenAnzhal());
		System.out.println("Schiffsname: " + r1.getSchiffname());
		
		
		//ROMULANER RAUMSCHIFF

		Raumschiff r2 = new Raumschiff();
		r2.setPhotonentorpedoAnzahl(2);
		r2.setEnergieversorgungInProzent(100);
		r2.setSchildeInProzent(100);
		r2.setHuelleInProzent(100);
		r2.setLebenserhaltungssystemeInProzent(100);
		r2.setAndroidenAnzahl(2);
		r2.setSchiffname("IRW Kahzara");

		System.out.println("Photonentorpedos: " + r2.getPhotonentorpedoAnzahl());
		System.out.println("Energieversorgung in Prozent: " + r2.getEnergieversorgungInProzent());
		System.out.println("Schilde in Prozent: " + r2.getSchildeInProzent());
		System.out.println("Huelle in Prozent: " + r2.getHuelleInProzent());
		System.out.println("Lebenserhaltungssyteme in Prozent: " + r2.getLebenserhaltungssystemeInProzent());
		System.out.println("Androiden Anzahl: " + r2.getAndroidenAnzhal());
		System.out.println("Schiffsname: " + r2.getSchiffname());
		
		
		//VULKANIER RAUMSCHIFF

		Raumschiff r3 = new Raumschiff();
		r3.setPhotonentorpedoAnzahl(0);
		r3.setEnergieversorgungInProzent(80);
		r3.setSchildeInProzent(80);
		r3.setHuelleInProzent(50);
		r3.setLebenserhaltungssystemeInProzent(100);
		r3.setAndroidenAnzahl(5);
		r3.setSchiffname("Ni'Var");

		System.out.println("Photonentorpedos: " + r3.getPhotonentorpedoAnzahl());
		System.out.println("Energieversorgung in Prozent: " + r3.getEnergieversorgungInProzent());
		System.out.println("Schilde in Prozent: " + r3.getSchildeInProzent());
		System.out.println("Huelle in Prozent: " + r3.getHuelleInProzent());
		System.out.println("Lebenserhaltungssyteme in Prozent: " + r3.getLebenserhaltungssystemeInProzent());
		System.out.println("Androiden Anzahl: " + r3.getAndroidenAnzhal());
		System.out.println("Schiffsname: " + r3.getSchiffname());
	}

}
