
public class Ladung {
	private String Bezeichnung;
	private int Menge;
	
	public Ladung() {
		this.Bezeichnung = "Unbekannt";
		this.Menge = 0;
	}
	public Ladung(String Bezeichnung, int Menge) {
		this.Bezeichnung = Bezeichnung;
		this.Menge = Menge;
	}
	
	public void setBezeichnung(String Bezeichnung) {
		this.Bezeichnung = Bezeichnung;
	}
	public String getBezeichnung() {
		return Bezeichnung;
	}
	public void setMenge(int Menge) {
		this.Menge = Menge;
	}
	public int getMenge() {
		return Menge;
	}
}
