import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.Box;
import javax.swing.BoxLayout;
public class JFrameVersuch {

	public static void main(String[] args) {
	JFrame Fahrkartenautomat = new JFrame();
		
		JPanel Arbeitsfläche = new JPanel();
		JPanel Tageskarten1 = new JPanel();
		
		
		JButton Tageskarten = new JButton("Tageskarten");
		JLabel FahrscheineT = new JLabel("Welchen Fahrschein möchten Sie kaufen?");
		JButton TageskarteBerlinAB = new JButton("Tageskarte Berlin AB");
		JButton TageskarteBerlinBC = new JButton("Tageskarte Berlin BC");
		JButton TageskarteBerlinABC = new JButton("Tagskarte Berlin ABC");
		
		JButton Einzelfahrscheine = new JButton("Einzelfahrscheine");
		JButton EinzelfahrscheinAB = new JButton("Einzelfahrschein AB");
		JButton EinzelfahrscheinBC = new JButton("Einzelfahrschein BC");
		JPanel Einzelfahrscheine1 = new JPanel();
		
		JButton Kurzstrecke = new JButton("Kurzstrecke");
						
		JButton KleingruppenTageskarten = new JButton("Kleingruppen-Tageskarten");
		
		
		
		Fahrkartenautomat.setTitle("Fahrkartenautomat");
		Fahrkartenautomat.setVisible(true);
		Fahrkartenautomat.setSize(1920, 1080);
		Fahrkartenautomat.setResizable(false);
				
				
		Fahrkartenautomat.add(Arbeitsfläche);
		Arbeitsfläche.setLayout(null);
		
				
		Arbeitsfläche.add(Tageskarten);
		Arbeitsfläche.add(Einzelfahrscheine);
		
		Arbeitsfläche.add(KleingruppenTageskarten);
		Arbeitsfläche.add(Kurzstrecke);
		
		Arbeitsfläche.add(Tageskarten1);
		Arbeitsfläche.add(FahrscheineT);
		
		Arbeitsfläche.add(Einzelfahrscheine1);
		Einzelfahrscheine1.add(EinzelfahrscheinAB);
		Einzelfahrscheine1.add(EinzelfahrscheinBC);
		
		Tageskarten1.add(TageskarteBerlinAB);
		Tageskarten1.add(TageskarteBerlinBC);
		Tageskarten1.add(TageskarteBerlinABC);
		
		Einzelfahrscheine1.setBounds(250, 500, 1200, 90);
		Einzelfahrscheine1.setVisible(false);
		
		Tageskarten1.setBounds(250, 500, 1200, 90);
		Tageskarten1.setVisible(false);
		
		FahrscheineT.setBounds(610, 450, 600, 40);
		FahrscheineT.setFont(new Font("FahrscheineT", 0, 30));
		FahrscheineT.setVisible(false);
		
		Kurzstrecke.setVisible(true);
		Kurzstrecke.setFont(new Font("Kurzstrecke", 0, 30));
					
		Tageskarten.setVisible(true);
		Tageskarten.setFont(new Font("Tageskarten", 0, 30));
		
		Einzelfahrscheine.setVisible(true);
		Einzelfahrscheine.setFont(new Font("Einzelfahrscheine", 0, 30));
		
		KleingruppenTageskarten.setVisible(true);
		KleingruppenTageskarten.setFont(new Font("Kleingruppen-Tageskarten", 0, 30));
		
		TageskarteBerlinAB.setFont(new Font("TageskarteAB", 0, 30));
		TageskarteBerlinBC.setFont(new Font("TageskarteBC", 0, 30));
		TageskarteBerlinABC.setFont(new Font("TageskarteABC", 0, 30));

		
		EinzelfahrscheinAB.setFont(new Font("Einzelfahrschein", 0, 30));
		EinzelfahrscheinBC.setFont(new Font("Eizelfahrschein", 0, 30));

		Tageskarten.setBounds(150, 50, 450, 100);
		Einzelfahrscheine.setBounds(620, 50, 450, 100);
		Kurzstrecke.setBounds(1090, 50, 450, 100);
		KleingruppenTageskarten.setBounds(620, 200, 450, 100);
		
		Tageskarten.addActionListener(e -> Tageskarten1.setVisible(true));
		Tageskarten.addActionListener(e -> FahrscheineT.setVisible(true));
		
		Einzelfahrscheine.addActionListener(e -> Einzelfahrscheine1.setVisible(true));
		Einzelfahrscheine.addActionListener(e -> Tageskarten1.setVisible(false));
		Einzelfahrscheine.addActionListener(e -> FahrscheineT.setVisible(false));
	}

}
