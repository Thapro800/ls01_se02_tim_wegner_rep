﻿import java.util.Scanner;

class Fahrkartenautomat
{
	public static void main(String[] args)
    {
		//ARRAY FAHKRTEN `
		
		String[] Fahrkarten = {"Einzelfahrschein AB", "Einzelfahrschein BC", "Einzelfahrschein ABC, Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte AB", "Kleingruppen-Tageskarte BC", "Kleingruppen-Tageskarte ABC"};
		double[] Fahrpreise = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
		
		//Fahrscheinerfassung 
		double fahrscheinpreis = 0;
		fahrscheinpreis = FahrscheinartErfassen (fahrscheinpreis, Fahrkarten, Fahrpreise);
		
		//Erfassung Anzahl Tickets, Berechnung des Gesamtpreises
		double gesamtpreis = 0; 
	    gesamtpreis = fahrkartenbestellungErfassen(fahrscheinpreis, gesamtpreis);
	    
	    
	    //Bezahlung der Fahrkarten, Geldeinwurf
	   double eingezahlterGesamtbetrag = 0;
	   double eingeworfeneMünze = 0;
	   double rückgabebetrag = 0;
	   rückgabebetrag = FahrkartenBezahlen (gesamtpreis, eingezahlterGesamtbetrag, eingeworfeneMünze, rückgabebetrag);
	    
	   // Fahrscheinausgabe
	   Fahrscheinausgabe ();
	   
	   // Rückgeldberechnung und -Ausgabe
	   Rückgeldberechnung (rückgabebetrag);
    }

		public static double FahrscheinartErfassen (double fahrscheinpreis, String[]Fahrscheine, double[]Fahrpreis) {
		Scanner Fahrscheinauswahl = new Scanner (System.in);
		System.out.println("Welche Art von Fahrschein möchten Sie kaufen? "
				+ "\n 1) Einzelfahrschein Berlin AB"
				+ "\n 2) Einzehlfahrschein Berlin BC"
				+ "\n 3) Einzelfahrschein ABC"
				+ "\n 4) Kurzstrecke"
				+ "\n 5) Tageskarte Berlin AB"
				+ "\n 6) Tageskarte Berlin BC"
				+ "\n 7) Tageskarte Berlin ABC"
				+ "\n 8) Kleingruppen-Tageskarten AB"
				+ "\n 9) Kleingruppen-Tageskarte BC"
				+ "\n 10) Kleingruppen-Tageskarte ABC");
		String Fahrscheinart = Fahrscheinauswahl.next();
		if (Fahrscheinart.equals("1")) {
			System.out.println("Sie haben eine/n " + Fahrscheine[0] + " gewählt, der Preis beträgt "+ "2.90€");
			fahrscheinpreis = Fahrpreis[0];
			return fahrscheinpreis;
		}
		if (Fahrscheinart.equals("2")) {
			System.out.println("Sie haben eine/n " + Fahrscheine[1] + " gewählt, der Preis beträgt "+ "3.30€");
			fahrscheinpreis = Fahrpreis[1];
			return fahrscheinpreis;
		}
		if (Fahrscheinart.equals("3")) {
			System.out.println("Sie haben eine/n " + Fahrscheine[2] + " gewählt, der Preis beträgt "+ "3.60€");
			fahrscheinpreis = Fahrpreis[2];
			return fahrscheinpreis;
		}
		if (Fahrscheinart.equals("4")) {
			System.out.println("Sie haben eine/n " + Fahrscheine[3] + " gewählt, der Preis beträgt "+ "1.90€");
			fahrscheinpreis = Fahrpreis[3];
			return fahrscheinpreis;
		}
		if (Fahrscheinart.equals("5")) {
			System.out.println("Sie haben eine/n " + Fahrscheine[4] + " gewählt, der Preis beträgt "+ "8.60€");
			fahrscheinpreis = Fahrpreis[4];
			return fahrscheinpreis;
		}
		if (Fahrscheinart.equals("6")) {
			System.out.println("Sie haben eine/n " + Fahrscheine[5] + " gewählt, der Preis beträgt "+ "9.00€");
			fahrscheinpreis = Fahrpreis[5];
			return fahrscheinpreis;
		}
		if (Fahrscheinart.equals("7")) {
			System.out.println("Sie haben eine/n " + Fahrscheine[6] + " gewählt, der Preis beträgt "+ "9.60€");
			fahrscheinpreis = Fahrpreis[6];
			return fahrscheinpreis;
		}
		if (Fahrscheinart.equals("8")) {
			System.out.println("Sie haben eine/n " + Fahrscheine[7] + " gewählt, der Preis beträgt "+ "23.50€");
			fahrscheinpreis = Fahrpreis[7];
			return fahrscheinpreis;
		}
		if (Fahrscheinart.equals("9")) {
			System.out.println("Sie haben eine/n " + Fahrscheine[8] + " gewählt, der Preis beträgt "+ "24.30€");
			fahrscheinpreis = Fahrpreis[8];
			return fahrscheinpreis;
		}
		if (Fahrscheinart.equals("10")) {
			System.out.println("Sie haben eine/n " + Fahrscheine[9] + " gewählt, der Preis beträgt "+ "24.90€");
			fahrscheinpreis = Fahrpreis[9];
			return fahrscheinpreis;
		}
		
		
		return fahrscheinpreis;
    }
		
		public static double fahrkartenbestellungErfassen(double fahrscheinpreis, double gesamtpreis) {
		
		Scanner fahrkartenanzahl = new Scanner (System.in);
		System.out.println("Wie viele Fahrkarten möchten Sie kaufen?");
		double anzahlfahrkarten = fahrkartenanzahl.nextDouble();
	    gesamtpreis = fahrscheinpreis * anzahlfahrkarten;
		return gesamtpreis;
		}

		public static double FahrkartenBezahlen (double gesamtpreis, double eingezahlterGesamtbetrag, double eingeworfeneMünze, double rückgabebetrag) {
			Scanner tastatur = new Scanner (System.in);
		       while(eingezahlterGesamtbetrag < gesamtpreis) 
		       {
		    	   System.out.println("Noch zu zahlen: " + (gesamtpreis - eingezahlterGesamtbetrag));
		    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
		       }
		       rückgabebetrag = eingezahlterGesamtbetrag - gesamtpreis;
	           return rückgabebetrag;
		}
			   
	    public static void Fahrscheinausgabe () {

	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			}
	          catch (InterruptedException e) {
				e.printStackTrace();
			}
	       }
	       
	       System.out.println("\n\n");
	       }
	       
	    public static void Rückgeldberechnung (double rückgabebetrag) {
	       
	       
	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	    	   while(rückgabebetrag >= 10.0) // 10 Euro-Schein
	    	   {
	    		   System.out.println("10 EURO");
	    		   rückgabebetrag -= 10.0;
	    	   }
	    	   
	    	   while(rückgabebetrag >= 5.0) // 5 Euro-Schein
	    	   {
	    		   System.out.println("5 EURO");
	    		   rückgabebetrag -=5.0;
	    	   }
	    	   
	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.");
	    }
}
		
		
		
		
		
		
		

	
