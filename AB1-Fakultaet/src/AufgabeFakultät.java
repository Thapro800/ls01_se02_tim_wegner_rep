

public class AufgabeFakultät {

	public static void main(String[] args) {
		System.out.printf("%-5s=","0!"); 
		System.out.printf("%23s","=");System.out.printf("%4s\n","1");

		
		System.out.printf ("%-5s= 1","1!");
		System.out.printf("%21s","=");System.out.printf("%4s\n","1");
		
		System.out.printf("%-5s= 1 * 2","2!"); 
		System.out.printf("%17s","=");System.out.printf("%4s\n","2");
		
		System.out.printf("%-5s= 1 * 2 * 3","3!"); 
		System.out.printf("%13s","=");System.out.printf("%4s\n","6");
		
		System.out.printf("%-5s= 1 * 2 * 3 * 4","4!"); 
		System.out.printf("%9s","=");System.out.printf("%4s\n","24");
		
		System.out.printf("%-5s= 1 * 2 * 3 * 4 * 5","5!"); 
		System.out.printf("%5s","=");System.out.printf("%4s\n","120");
		
	}

}
