import java.util.ArrayList;
import java.util.Scanner;

public class Benutzerverwaltung {

	public static void main(String[] args) {
		
		
		ArrayList<Benutzer> benutzerliste = new ArrayList<Benutzer>();
		Benutzer b1 = new Benutzer(1, "Tim", "Wegner", 2001);
		benutzerliste.add(b1);
				
		int auswahl;
		
		do {
			auswahl = menu();
			switch (auswahl) {
			case 1:
				// benutzerAnzeigen();
				System.out.println(benutzerliste);
				break;
			case 2:
				// bentzerErfassen();
				bentzerErfassen(benutzerliste);
				break;
			case 3:
				// benutzerLöschen();
				break;
			case 4:
				System.exit(0);
			default:
				System.err.println("\nFalsche Eingabe.\n");				
			}
		}while (true );
		
	}
	
	public static int menu() {

        int selection;
        Scanner input = new Scanner(System.in);

        /***************************************************/

        System.out.println("     Benutzerverwaltung     ");
        System.out.println("----------------------------\n");
        System.out.println("1 - Benutzer anzeigen");
        System.out.println("2 - Benutzer erfassen");
        System.out.println("3 - Benutzer löschen");
        System.out.println("4 - Ende");
         
        System.out.print("\nEingabe:  ");
        selection = input.nextInt();
        return selection;    
    }
	public static void bentzerErfassen(ArrayList<Benutzer> benutzerliste) {
		Scanner tastatur = new Scanner(System.in);
		int benutzernummer;
		System.out.print("Bitte geben Sie die Benutzernummer ein: ");
		benutzernummer = tastatur.nextInt();

		String vorname;
		System.out.print("Bitte geben Sie den Vornamen ein: ");
		vorname = tastatur.next();

		String nachname;
		System.out.print("Bitte geben Sie den Nachnamen ein: ");
		nachname = tastatur.next();

		int geburtsjahr;
		System.out.print("Bitte geben Sie das Geburtsjahr ein: ");
		geburtsjahr = tastatur.nextInt();
		
		Benutzer b1 = new Benutzer(benutzernummer, vorname, nachname, geburtsjahr);
		benutzerliste.add(b1);

		
	}
}
