import java.util.ArrayList;

public class ArrayListBeispiele {

	public static void main(String[] args) {
		
	ArrayList<String> namenliste = new ArrayList<String>();
	
	namenliste.add("Max");
	namenliste.add("Alex");
	
	
	System.out.println(namenliste);

	namenliste.remove(1);
	
	System.out.println(namenliste);
	
	
	ArrayList<Buch> buchliste = new ArrayList<Buch>();
	
	Buch b1 = new Buch("OOP", 20.99);
	
	buchliste.add(b1);
	
	System.out.println(buchliste);
	}

}
